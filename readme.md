Account 360 is an application package that supports Business to Business operations. It includes a pre-defined data model, functionality and components. It is a software solution that can be used to manage Organizations, Contacts and their affiliations by providing a single version of truth. The solution helps to centralize and master customer data in order to manage customer relationships effectively. Optional components of the Account 360 solution provide integration with Salesforce and Dun and Bradstreet to further enrich the customer data.
It is designed to be extensible, but still approachable using the default configuration.


## Change Log

```

Last Update Date: 10/04/2018

Version: 2018.DDA.10
Description:
1) Bug fix: Contact attributes like 'Start Year' formatted as a Number with thousand separator (DDA-1213)
2) Ability to work with reference and nested addess simultaneously in Data Tenant (DDA-1207)
```

## Contributing
Please visit our [Conitributor Covenant Code of Conduct](https://bitbucket.org/reltio-ondemand/common/src/master/CodeOfConduct.md) to learn more about our contribution guidlines

## Licensing
```
Copyright (c) 2018 Reltio



Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at



    http://www.apache.org/licenses/LICENSE-2.0



Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

See the License for the specific language governing permissions and limitations under the License.
```

## Quick Start
For additional details about the Account 360 application see [here](https://bitbucket.org/reltio-ondemand/app-account360/src//Data%20Model/?at=master).


### Repository Owners

[Terence Kirk](terence.kirk@reltio.com)

[Yuriy Raskin](yuriy.raskin@reltio.com)


